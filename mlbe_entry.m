%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distributed under GNU General Public License (see license.txt for details).
%
% Copyright (c) 2012 Linus ZHEN Yi
% All Rights Reserved.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The entry function for MLBE
% Input:
%   methodname: 'cvh', 'mlbe'
%   nclass: # of classes used
%   lmperClass: # of training per class
%   clength: length of code
%   repid: id of training set
%   evalrank: rank position for evaluation
%   evalradius: radius for evaluation
% Usage:
%   mlbe_entry('mlbe', 8, 1, 50);
% Saved Result:
%   Probresult{1}.XY Probresult{1}.YX : precision w.r.t. hamming radius
%   Probresult{2}.XY Probresult{2}.YX : recall w.r.t. hamming radius
%   Probresult{3}.XY Probresult{3}.YX : # retrieved points w.r.t. hamming radius
%   Probresult{4}.XY Probresult{4}.YX : precision w.r.t. hamming rank
%   Probresult{5}.XY Probresult{5}.YX : recall w.r.t. hamming rank
%   Probresult{6}.XY Probresult{6}.YX : map w.r.t. hamming rank
%   Probparam                         : parameters
%   hyper                             : hyper parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function mlbe_entry(methodname, clength, nrep, evalrank)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Choose data files
    Probparam.datafile = './data/UCSD_TT10';
    Probparam.simfile = './data/UCSD_SIMEUC10_RNNG';
    Probparam.rdidxfile = './data/UCSD_CTIDX10_30';
    Probparam.obfile = './data/UCSD_OB10_0.0010.mat';
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Set parameters
    addpath utils;
    Probparam.hammingrank = evalrank;% for evaluation
    Probparam.codelength = clength;
    Probparam.repeats = nrep; % repeats of landmark index
    Probparam.methodname = methodname;  % 'cvh', 'cmssh', 'cvh-mlbe'
%     Probparam

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Perform experiments
    fprintf('Program starts running...');
    [Probresult, hyper] = mlbe_main(Probparam);

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Save results
    save(sprintf('./results/projmlbe_res_%s_%db.mat', methodname, clength),'Probresult','Probparam','hyper');
    fprintf('ends.\n');
end