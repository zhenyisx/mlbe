function X = generate_random_set(Labels,sigma1,sigma2,nn,dim)

N = length(Labels);
ul = unique(Labels);

X = zeros(dim,N);


for k = 1:length(ul)
    ll = find(Labels == ul(k));
    
    c = randn(dim,1)*sigma1;
    x = randn(dim,length(ll));
    r = randperm(dim);
    r = r(1:nn);
    x(r,:) = x(r,:)*sigma2;
    
    X(:,ll) = x + repmat(c,[1 size(x,2)]);
end

