#include <mex.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define MIN(a,b) ( (a)<(b) ? (a) : (b) )
#define MAX(a,b) ( (a)>(b) ? (a) : (b) )
#define MAX3(a,b,c) ( (a)>(b) & (a)>(c) ? (a) : (b)>(a) & (b)>(c) ? (b) : (c) )


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   
    int i;
	mwSize dims[3];
    unsigned int *v = mxGetData(prhs[0]);
    double *w = mxGetData(prhs[1]);

    int L = ((double *)mxGetData(prhs[2]))[0];
    double  *c;
    int len = mxGetM(prhs[0]) * mxGetN(prhs[0]);

	dims[0] = L;
	dims[1] = 1;
	plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
    c       = mxGetData(plhs[0]);
   
	for (i=0; i<L; i++) { c[i]=0.0; }
	for (i=0; i<len; i++) { c[*v++] += *w++; }
    
}



