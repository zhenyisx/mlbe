function c = correlation(X,Y)
nx = sum(X.^2, 1);
ny = sum(Y.^2, 1);
c  = sum(X.*Y,1)./sqrt(nx.*ny);