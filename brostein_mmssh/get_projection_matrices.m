function [PX,PY, v] = get_projection_matrices(dataset)

assert(isa(dataset,'HashDataSet'),'dataset does not appear to be a Hash_DataSet');

% dim = 81;
% Xp = zeros(dim, size(similar,1));
% Yp = zeros(dim, size(similar,1));
% Xn = zeros(dim, size(dissimilar,1));
% Yn = zeros(dim, size(dissimilar,1));



% np = size(xp,1); 
% nn = size(xn,1); 

Xp = dataset.m_similar.m_featSetMod1';
Yp = dataset.m_similar.m_featSetMod2';

Xn = dataset.m_dissimilar.m_featSetMod1';
Yn = dataset.m_dissimilar.m_featSetMod2';

% countp = 0;
% countn = 0;
% for k=1:10,
%     load(fullfile(DIR, sprintf('T1T2.81dim.%d.mat', k)));
%     xp = eval(sprintf('similar%d', k));
%     xn = eval(sprintf('dissimilar%d', k));
%     eval(sprintf('clear similar%d', k));
%     eval(sprintf('clear dissimilar%d', k));
%     np = size(xp,1); 
%     nn = size(xn,1); 
%     Xp(:,countp+[1:np]) = xp(:,1:end/2)';
%     Yp(:,countp+[1:np]) = xp(:,end/2+1:end)';
%     countp = countp + np;
%     Xn(:,countn+[1:nn]) = xn(:,1:end/2)';
%     Yn(:,countn+[1:nn]) = xn(:,end/2+1:end)';
%     countn = countn + nn;
%     [countp countn]
% end
% Xp = Xp(:,1:countp);
% Yp = Yp(:,1:countp);
% Xn = Xn(:,1:countn);
% Yn = Yn(:,1:countn);
Np = size(Xp,2);
Nn = size(Xn,2);
idxp = randperm(Np);
idxn = randperm(Nn);


% Training settings (equal for all algorithms)
M = 64;
K = [1000 15 300];
Np_train = floor(0.4*Np);
Nn_train = floor(0.4*Nn);
Np_test  = floor(0.6*Np);
Nn_test  = floor(0.6*Nn);

% Similarity on X
Xp_train = Xp(:,idxp(1:Np_train));
Yp_train = Yp(:,idxp(1:Np_train));
Xn_train = Xn(:,idxn(1:Nn_train));
Yn_train = Yn(:,idxn(1:Nn_train));
Xp_test  = Xp(:,idxp([1:Np_test]+Np_train));
Yp_test  = Yp(:,idxp([1:Np_test]+Np_train));
Xn_test  = Xn(:,idxn([1:Nn_test]+Nn_train));
Yn_test  = Yn(:,idxn([1:Nn_test]+Nn_train));

[PX,PY, v] = run_adaboost_cross(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K(2:end),M);




