function dp_test = hamming_distances(Xp_test,Yp_test,projx,projy)

if nargin < 4,
    projy = projx;
end

xp_test = sign(bsxfun(@plus, projx(:,1:end-1)*Xp_test, projx(:,end)));
yp_test = sign(bsxfun(@plus, projy(:,1:end-1)*Yp_test, projy(:,end)));
dp_test = cumsum(double(xp_test ~= yp_test),1);
