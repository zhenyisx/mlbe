function [projx,projy, xp_best,yp_best,xn_best,yn_best, alpha, val] = adaboost_iter_cross(Xp,Yp,Xn,Yn,wp,wn,K,L, alpha, loss)

[U,V] = optimal_subspace_cross(Xp,Yp,Xn,Yn,wp,wn, K);

% Add random directions
if L-size(U,2)>0,
    U = [U , U*randn(size(U,2), L-size(U,2))];
end
if L-size(V,2)>0,
    V = [V , V*randn(size(V,2), L-size(V,2))];
end

str = mprintf('', '   Optimizing threshold...');
best_val = Inf;
projx = [];
projy = [];
xp_best = [];
yp_best = [];
xn_best = [];
yn_best = [];
for i=1:size(V,2),
    % Do gap optimization to find best threshold
    % for projection on the selected direction
    u = U(:,i);
    v = V(:,i);
    if ~isempty(Xp), xp = u'*Xp;else xp=[];end
    if ~isempty(Yp), yp = v'*Yp;else yp=[];end
    if ~isempty(Xn), xn = u'*Xn;else xn=[];end
    if ~isempty(Yn), yn = v'*Yn;else yn=[];end
    [thx, thy, val] = best_threshold2d(xp, yp, xn, yn, wp, wn, alpha, loss);
    if val < best_val,
        best_val = val;
        projx = [u', -thx];
        projy = [v', -thy];
        xp_best = xp - thx;
        yp_best = yp - thy;
        xn_best = xn - thx;
        yn_best = yn - thy;
    end
end

% Make sure that 0 vector always has 0 bits
% by flipping projection sign
%if proj(end) > 0, proj = -proj; end

str = mprintf(str, '');

