function pairs = generate_pairs(Labels_te,K,rptflag)

ul = unique(Labels_te);
pairs = [];

for k = 1:length(ul)
    ll = find(Labels_te == ul(k));
    ll = ll(randperm(length(ll)));
    ll = ll(1:min(K,length(ll)));
    [i j] = generate_couples(length(ll),0);
    pairs = [pairs; ll(i) ll(j)];
end



function [i j] = generate_couples(N,rptflag)

i = [];
j = [];

if rptflag

    for k = 1:N
        for l = k+1:N
            i = [i; k];
            j = [j; l];
        end
    end

else

    for k = 1:N
        for l = 1:N
            i = [i; k];
            j = [j; l];
        end
    end
    
    
end