function [Px,Py] = optimal_subspace_cross(Xp,Yp,Xn,Yn,wp,wn, K,spherize)

if length(K) < 2,   K = [K K];          end
if nargin < 8,      spherize = true;    end

% Spherize data
if spherize, 
    str = mprintf('', '   Spherizing data...');
    [X,Ux] = spherizew([Xp Xn],[wp(:); wn(:)], 0.99, K(1), 1e-10);
    [Y,Uy] = spherizew([Yp Yn],[wp(:); wn(:)], 0.99, K(2), 1e-10);
else
    str = mprintf('', '   Centering data...');
    [X,Ux] = centerw([Xp Xn],[wp(:); wn(:)]);
    [Y,Uy] = centerw([Yp Yn],[wp(:); wn(:)]);    
end

str = mprintf(str, '   Computing covariance matrices...');

% Weight data
X = bsxfun(@times, X, sqrt([wp(:); wn(:)]'));
Y = bsxfun(@times, Y, sqrt([wp(:); wn(:)]'));

% Weighted covariance
if sum(wp) > 0
    Cp = X(:,1:length(wp))*Y(:,1:length(wp))' / sum(wp);
else
    Cp = 0;
end
if sum(wn) > 0
    Cn = X(:,length(wp)+1:end)*Y(:,length(wp)+1:end)' / sum(wn);
else
    Cn = 0;
end
 
str = mprintf('', '   Computing optimal subspace...');

% Optimal sub-space
[U,S,V] = svd(Cp-Cn);
Px = Ux*U(:,1:min(size(U,2),K(1)));
Py = Uy*V(:,1:min(size(V,2),K(2)));

str = mprintf(str, '');

return;

% In place positive covariance
dp = Xp-Yp;
dp = bsxfun(@times, dp, sqrt(wp(:)'));
if dimsub == dim, 
    Cp = double(dp*dp');
else
    cp = sum(dp.^2, 2);
end

% In place negative covariance
dn = Xn-Yn;
dn = bsxfun(@times, dn, sqrt(wn(:)'));
if dimsub == dim, 
    Cn = double(dn*dn');
else
    cn = sum(dn.^2, 2);
end


if dimsub < dim, 

    str = mprintf(str, '   Computing covariance matrices...');
    
    % Compute subspace maximizing ratio
    warning off;
    r = cn./cp;
    warning on;
    r(cp==0) = 0;
    [r, idx] = sort(r, 'descend');

    idx = idx(1:dimsub);

    Cp = double(dp(idx,:)*dp(idx,:)');
    Cn = double(dn(idx,:)*dn(idx,:)');
    
else
    
    idx = 1:dim;
    
end



% Compute inverse square root of Cp
str = mprintf(str, '   Computing %d-dimensional subspace....', K);
[Vp, Dp] = eig(Cp);
warning off;
dp = sqrt(diag(Dp));
dpmin = max(1e-10, 1e-4*max(dp));
dp(dp<dpmin) = 0;
dp = 1./dp;
dp(isinf(dp)) = 0;
warning on;
Cp = Vp*diag(dp)*Vp';   % Cp^(-0.5)

% Compute K-dimensional subspace based on largest eigenspace of
% Cp^(-0.5)*Cn*Cp^(-0.5)
[V, d] = eigs(Cp*Cn*Cp, K, 'lm', struct('disp', 0));
V = Cp*V;
d = diag(d);

% Final subspace projection matrix for original data
P = zeros(dim, K);
for k=1:K,
    P(idx,k) = V(:,k);
end

str = mprintf(str, '');