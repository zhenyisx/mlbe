function [th,val,alpha] = best_threshold(xp,yp,xn,yn, wp,wn, alpha, loss)

np = length(xp);
nn = length(xn);
x = [xp(:)' xn(:)' yp(:)' yn(:)'];
w = [wp(:)' wn(:)' wp(:)' wn(:)'];

[th,idx] = sort(x,'ascend');
th = [th(1)-1; th(:)];
delta = [th(2:end)-th(1:end-1); 0];
th = th + delta;
LUT = zeros(length(th),1);
LUT(idx) = 2:length(idx)+1;


ixp = LUT(1:np);
iyp = LUT(np+nn+[1:np]);
ixn = LUT(np+[1:nn]);
iyn = LUT(np+nn+np+[1:nn]);

fp1 = zeros(length(th),1);
fp2 = zeros(length(th),1);
fp1(min(ixp,iyp)) = wp;
fp2(max(ixp,iyp)) = wp;

fn1 = zeros(length(th),1);
fn2 = zeros(length(th),1);
fn1(min(ixn,iyn)) = wn;
fn2(max(ixn,iyn)) = wn;


% Loss function
switch lower(loss),
    
    case 'exp'
        Efp = exp(+alpha);
        Etp = exp(-alpha);
        Etn = exp(-alpha);
        Efn = exp(+alpha);
        
    case 'quad'
    
        Efp = 0;
        Etp = -1;
        Etn = -1;
        Efn = 0;
        
    otherwise
        
        throw(MException('ParseOptions:Unsupported', 'Loss function unsupported'));
        
end        


    Ep  = Etp*sum(wp) + (Efp-Etp)*cumsum(fp1) + (Etp-Efp)*cumsum(fp2);
    En  = Efn*sum(wn) + (Etn-Efn)*cumsum(fn1) + (Efn-Etn)*cumsum(fn2);

    [Emin, idxmin] = min((Ep+En)/2);
    thmin = th(idxmin);
    
    
% if false,    
%     xp_ = sign(xp - thmin);
%     yp_ = sign(yp - thmin);
%     xn_ = sign(xn - thmin);
%     yn_ = sign(yn - thmin);
% 
%     tp = sum(wp(:).*double(xp_(:) == yp_(:)));
%     fn = sum(wp(:).*double(xp_(:) ~= yp_(:)));
%     tn = sum(wn(:).*double(xn_(:) == yn_(:)));
%     fp = sum(wn(:).*double(xn_(:) ~= yn_(:)));
% 
%     warning off;
%     alpha     = 0.5*log((tp+tn)/(fp+fn));
%     warning on;
% end
    
th = thmin;
val = Emin;



return



np = length(xp);
nn = length(xn);
x = [xp(:)' xn(:)' yp(:)' yn(:)'];
w = [wp(:)' wn(:)' wp(:)' wn(:)'];
%l = [ones(1,np) -ones(1,nn) ones(1,np) -ones(1,nn)];

[th,idx] = sort(x,'ascend');
th = [th(1)-1; th(:)];
delta = [th(2:end)-th(1:end-1); 0];
th = th + delta;
LUT = zeros(length(th),1);
LUT(idx) = 2:length(idx)+1;

% False positives
ixn = LUT(np+[1:nn]);
iyn = LUT(np+nn+np+[1:nn]);
fp1 = zeros(length(th),1);
fp1(max(ixn,iyn)) = wn;
fp1 = cumsum(fp1);
fp2 = zeros(length(th),1);
fp2(min(ixn,iyn)-1) = wn;
fp2 = sum(fp2)-cumsum(fp2); 
tp  = 1-(fp1 + fp2);

% False negatives
ixp = LUT(1:np);
iyp = LUT(np+nn+[1:np]);
fn1 = zeros(length(th),1);
fn1(max(ixp,iyp)) = wp;
fn1 = cumsum(fn1);
fn2 = zeros(length(th),1);
fn2(min(ixp,iyp)-1) = wp;
fn2 = sum(fn2)-cumsum(fn2); 
tn  = fn1 + fn2;

%figure(2); plot(th,tp+tn, th__,Ep+En);

[val,i] = max((tp+tn)/2);
th = th(i);
fp = 1-tp(i);
fn = 1-tn(i);

%[val th]
%if abs(th-th0) > 0.1,
%    pause;
%end




return;
%xp = Xp(1,:);
%yp = Yp(1,:);
%xn = Xn(1,:);
%yn = Yn(1,:);

np = length(xp);
nn = length(xn);
x = [xp(:)' xn(:)' yp(:)' yn(:)'];
w = [wp(:)' wn(:)' wp(:)' wn(:)'];
%l = [ones(1,np) -ones(1,nn) ones(1,np) -ones(1,nn)];

[th,idx] = sort(x,'ascend');
th = [th(1)-1; th(:)];
delta = [th(2:end)-th(1:end-1); 0];
th = th + delta;
LUT = zeros(length(th),1);
LUT(idx) = 2:length(idx)+1;

% False positives
ixn = LUT(np+[1:nn]);
iyn = LUT(np+nn+np+[1:nn]);
fp1 = zeros(length(th),1);
fp1(max(ixn,iyn)) = wn;
fp1 = cumsum(fp1);
fp2 = zeros(length(th),1);
fp2(min(ixn,iyn)-1) = wn;
fp2 = sum(fp2)-cumsum(fp2); 
tp  = 1-(fp1 + fp2);

% False negatives
ixp = LUT(1:np);
iyp = LUT(np+nn+[1:np]);
fn1 = zeros(length(th),1);
fn1(max(ixp,iyp)) = wp;
fn1 = cumsum(fn1);
fn2 = zeros(length(th),1);
fn2(min(ixp,iyp)-1) = wp;
fn2 = sum(fn2)-cumsum(fn2); 
tn  = fn1 + fn2;

[val,i] = max((tp+tn)/2);
th = th(i);
fp = 1-tp(i);
fn = 1-tn(i);



return;
fn_ = fn*0;
fp_ = fp*0;
for k=1:length(fn_),
    %idx = find( sign(xp-th(k)) .* sign(yp-th(k)) > 0);
    %idx = find( sign(xp-th(k)) > 0 & sign(yp-th(k)) > 0);
    %fn_(k) = sum(wp(idx));
    idx = find( sign(xn-th(k)) .* sign(yn-th(k)) < 0);
    %idx = find( sign(xn-th(k)) > 0 | sign(yn-th(k)) > 0);
    fp_(k) = sum(wn(idx));
end




np = length(xp);
nn = length(xn);
x = [xp(:)' xn(:)' yp(:)' yn(:)'];
w = [wp(:)' wn(:)' wp(:)' wn(:)'];

[th,idx] = sort(x,'ascend');
th = [th(1)-1; th(:)];
delta = [th(2:end)-th(1:end-1); 0];
th = th + delta;
LUT = zeros(length(th),1);
LUT(idx) = 2:length(idx)+1;


ixp = LUT(1:np);
iyp = LUT(np+nn+[1:np]);
ixn = LUT(np+[1:nn]);
iyn = LUT(np+nn+np+[1:nn]);

fp1 = zeros(length(th),1);
fp2 = zeros(length(th),1);
fp1(min(ixp,iyp)) = wp;
fp2(max(ixp,iyp)) = wp;

fn1 = zeros(length(th),1);
fn2 = zeros(length(th),1);
fn1(min(ixn,iyn)) = wn;
fn2(max(ixn,iyn)) = wn;


alpha0    = 0; %0.5;
maxiter   = 1; %10;
minchange = 1e-3;

alpha     = alpha0;
    
for k=1:maxiter,

    Efp = exp(+alpha);
    Etp = exp(-alpha);
    Etn = exp(-alpha);
    Efn = exp(+alpha);
    
    Efp = 0;
    Etp = -1;
    Etn = -1;
    Efn = 0;

    Ep  = Etp*sum(wp) + (Efp-Etp)*cumsum(fp1) + (Etp-Efp)*cumsum(fp2);
    En  = Efn*sum(wn) + (Etn-Efn)*cumsum(fn1) + (Efn-Etn)*cumsum(fn2);

    [Emin, idxmin] = min(Ep+En);
    thmin = th(idxmin);

    xp_ = sign(xp - thmin);
    yp_ = sign(yp - thmin);
    xn_ = sign(xn - thmin);
    yn_ = sign(yn - thmin);

    tp = sum(wp(:).*double(xp_(:) == yp_(:)));
    fn = sum(wp(:).*double(xp_(:) ~= yp_(:)));
    tn = sum(wn(:).*double(xn_(:) == yn_(:)));
    fp = sum(wn(:).*double(xn_(:) ~= yn_(:)));

    alpha_old = alpha;
    warning off;
    alpha     = 0.5*log((tp+tn)/(fp+fn));
    warning on;
    dalpha    = abs(alpha_old - alpha);

    if dalpha < minchange, break; end
    if alpha < 0 | isnan(alpha) | isinf(alpha),
        alpha = alpha0;
        break;
    end

end

alpha = 0.2;
th = thmin;
val = Emin;
