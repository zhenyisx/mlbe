function [P] = optimal_subspace(Xp,Yp,Xn,Yn,wp,wn, dimsub, K)

dim = size(Xp,1);
if nargin < 7, dimsub = dim; end
dimsub = max(min(dimsub, dim), 1);

if nargin < 8, K = 10; end
K = min(K, dimsub);

if dimsub < dim, 
    str = mprintf('', '   Computing %d-dimensional subspace...', dimsub);
else
    str = mprintf('', '   Computing covariance matrices...');
end

% In place positive covariance
dp = Xp-Yp;
dp = bsxfun(@times, dp, sqrt(wp(:)'));
if dimsub == dim, 
    Cp = double(dp*dp');
else
    cp = sum(dp.^2, 2);
end

% In place negative covariance
dn = Xn-Yn;
dn = bsxfun(@times, dn, sqrt(wn(:)'));
if dimsub == dim, 
    Cn = double(dn*dn');
else
    cn = sum(dn.^2, 2);
end


if dimsub < dim, 

    str = mprintf(str, '   Computing covariance matrices...');
    
    % Compute subspace maximizing ratio
    warning off;
    r = cn./cp;
    warning on;
    r(cp==0) = 0;
    [r, idx] = sort(r, 'descend');

    idx = idx(1:dimsub);

    Cp = double(dp(idx,:)*dp(idx,:)');
    Cn = double(dn(idx,:)*dn(idx,:)');
    
else
    
    idx = 1:dim;
    
end



% Compute inverse square root of Cp
str = mprintf(str, '   Computing %d-dimensional subspace....', K);
[Vp, Dp] = eig(Cp);
warning off;
dp = sqrt(diag(Dp));
dpmin = max(1e-10, 1e-4*max(dp));
dp(dp<dpmin) = 0;
dp = 1./dp;
dp(isinf(dp)) = 0;
warning on;
Cp = Vp*diag(dp)*Vp';   % Cp^(-0.5)

% Compute K-dimensional subspace based on largest eigenspace of
% Cp^(-0.5)*Cn*Cp^(-0.5)
[V, d] = eigs(Cp*Cn*Cp, K, 'lm', struct('disp', 0));
V = Cp*V;
d = diag(d);

% Final subspace projection matrix for original data
P = zeros(dim, K);
for k=1:K,
    P(idx,k) = V(:,k);
end

str = mprintf(str, '');