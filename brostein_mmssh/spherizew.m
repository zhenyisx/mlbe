function [xp,V] = spherizew(Xp, wp, maxenergy, maxsubspace, minvar)

if nargin < 3, maxenergy = 0.99;    end
if nargin < 4, maxsubspace = Inf;   end
if nargin < 5, minvar = 1e-8;       end

% Compute mean
xp = sum( bsxfun(@times, Xp, wp(:)'), 2 ) / sum(wp);

% Center data
Xp_ = bsxfun(@minus, Xp, xp);

% Weight data
Xpw = bsxfun(@times, Xp_, sqrt(wp(:)'));

% Compute covariance
Cp = Xpw * Xpw' / sum(wp);

% PCA
[V,d] = eig(Cp);
d = diag(d);
[d,idx] = sort(d,'descend');
dd = cumsum(d); dd = dd/dd(end);

% Find sub-space
i = find(dd>maxenergy & d>minvar);
i = i(1);
i = min(maxsubspace, i);
idx = idx(1:i);
d = d(1:i);
V = V(:,idx);


% Scale data
s = 1./sqrt(d);
V = bsxfun(@times, V, s(:)');

xp = V'*Xp_;

