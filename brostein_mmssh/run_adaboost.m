function [PROJ,variance] = run_adaboost(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K,M,alpha, loss)

% Initialize
np = size(Xp_train,2);
nn = size(Xn_train,2);
d  = size(Xp_train,1);  % Data dimensionality

use_testset   = ~isempty(Xp_test);
initial_rates = false;
show_plot     = false;

wp = ones(np,1); wp = wp/sum(wp);
wn = ones(nn,1); wn = wn/sum(wn);

if nargin < 9,
    K = [1000 10 50];              % Subspace size + number of directions to search
end
if nargin < 10,
    M = 64;             % Code size
end
if nargin < 11,
    alpha = .1;         % Reweighting speed
end
if nargin < 12,
    loss = 'quad';
end


PROJ = zeros(M, d+1);
variance = zeros(M,1);

mprintf('', 'Training %d-bit code   dimension: %d\n', M, d);

dp_train  = zeros(1, size(Xp_train,2));
dn_train  = zeros(1, size(Xn_train,2));
if initial_rates
    str = mprintf('', '   Evaluating initial training error...'); 
    d0p_train = 0.5+0.5*correlation(Xp_train,Yp_train);
    d0n_train = 0.5+0.5*correlation(Xn_train,Yn_train);
    [eer_train,fpr1_train,fpr01_train] = calculate_rates(d0p_train, d0n_train);
    mprintf(str, '  Input  Train:  eer %6.2f%%   fp@fn=1%% %6.2f%%   fp@fn=0.1%% %6.2f%%\n', 100*eer_train, 100*fpr1_train, 100*fpr01_train);
end

if use_testset,
    dp_test  = zeros(1, size(Xp_test,2));
    dn_test  = zeros(1, size(Xn_test,2));
    if initial_rates
        str = mprintf('', '   Evaluating initial test error...'); 
        d0p_test = 0.5+0.5*correlation(Xp_test,Yp_test);
        d0n_test = 0.5+0.5*correlation(Xn_test,Yn_test);
        [eer_test, fpr1_test, fpr01_test]  = calculate_rates(d0p_test,  d0n_test);
        mprintf(str, '         Test:   eer %6.2f%%   fp@fn=1%% %6.2f%%   fp@fn=0.1%% %6.2f%%\n', 100*eer_test, 100*fpr1_test, 100*fpr01_test);
    end
end

sum_alpha = 0;
for m = 1:M,

    tic;
    % Find best projection
    [proj, xp,yp,xn,yn, alpha] = adaboost_iter(Xp_train,Yp_train,Xn_train,Yn_train,wp,wn,K(1),K(2),K(3), alpha, loss);

    % Add to projection matrices
    proj = proj(:)';
    PROJ(m,:) = proj;
    
    % Compute variance
    variance(m) = 0.5*(mean(xn.^2) + mean(yn.^2));
    
    % Training rates
    str = mprintf('', '   Evaluating training error...'); 
    xp = sign(xp);
    yp = sign(yp);
    xn = sign(xn);
    yn = sign(yn);
    sum_alpha = sum_alpha + alpha;
    dp_train  = dp_train + alpha*double(xp ~= yp);
    dn_train  = dn_train + alpha*double(xn ~= yn);
    [eer_train,fpr1_train,fpr01_train, dee_train,dfr1_train,dfr01_train, dist_train,fp_train,fn_train] = ...
        calculate_rates(dp_train/sum_alpha, dn_train/sum_alpha, []);

    % Test rates
    if use_testset,
        str = mprintf(str, '   Evaluating test error...'); 
        xp_test = sign(proj(1:end-1)*Xp_test + proj(end));
        yp_test = sign(proj(1:end-1)*Yp_test + proj(end));
        xn_test = sign(proj(1:end-1)*Xn_test + proj(end));
        yn_test = sign(proj(1:end-1)*Yn_test + proj(end));
        dp_test = dp_test + alpha*double(xp_test ~= yp_test);
        dn_test = dn_test + alpha*double(xn_test ~= yn_test);   
        [eer_test, fpr1_test, fpr01_test,  dee_test, dfr1_test, dfr01_test,  dist_test, fp_test, fn_test]  = ...
            calculate_rates(dp_test/sum_alpha, dn_test/sum_alpha, []);
    end    

    % Output rates
    str = mprintf(str, '   %-3d variance %6.2f  elapsed time %s\n', m, variance(m), format_time(toc));
    mprintf('', '         Train:  eer %6.2f%%   fp@fn=1%% %6.2f%%   fp@fn=0.1%% %6.2f%%\n', 100*eer_train, 100*fpr1_train, 100*fpr01_train);
    if use_testset,
        mprintf('', '         Test:   eer %6.2f%%   fp@fn=1%% %6.2f%%   fp@fn=0.1%% %6.2f%%\n', 100*eer_test, 100*fpr1_test, 100*fpr01_test);
    end
    
    % Show plot
    if show_plot,
        if use_testset,
             plot(dist_train, fp_train, 'r',  dist_train, fn_train, 'b', ...
                  dist_test,  fp_test,  ':r', dist_test,  fn_test,  ':b');
             legend('FP (Train)', 'FN (Train)', 'FP (Test)', 'FN (Test)');
        else
             plot(dist_train, fp_train, 'r', dist_train, fn_train, 'b');
             legend('FP (Train)', 'FN (Train)');
        end
        axis([0 1 0 1]);
        xlabel('Normalized distance');
        drawnow;
    end

    % Reweight examples: 
    % - correctly classified ones are downweighted
    % - incorrectly classified ones are boosted
    %fprintf(1, '  Reweighting examples with alpha=%.4f\n', alpha); 
    dwp = exp(-alpha*(xp.*yp));
    dwn = exp(+alpha*(xn.*yn)); 
    wp  = wp(:) .* dwp(:);
    wn  = wn(:) .* dwn(:);
    wp  = wp / sum(wp);
    wn  = wn / sum(wn);
    
end
% End greedy adaboost iterations



