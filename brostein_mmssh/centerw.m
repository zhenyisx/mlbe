function [xp,V] = centerw(Xp, wp)

% Compute mean
xp = sum( bsxfun(@times, Xp, wp(:)'), 2 ) / sum(wp);

% Center data
xp = bsxfun(@minus, Xp, xp);

V = eye(size(Xp,1));
