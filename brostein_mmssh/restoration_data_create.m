
MAX_SAMPLES = 1e5;
MAX_SAMPLES_PER_IMAGE = 2e2;

IMG_DIR = 'C:\work\AffineSSBoFs\data\images\Inria';

dirlist = dir(fullfile(IMG_DIR, '*.jpg'));


% Kernel 
%h1 = [0 0 0 0 0 0 1 0 0;
%      0 0 1 1 1 1 0 1 0;
%      1 1 0 0 0 0 0 1 0;
%      0 0 0 0 0 0 0 0 1];
%h1 = [1 1 1 0 0;
%      0 0 0 1 1];
h1 = [0 0 0 0 1 0 0;
      0 0 1 1 0 1 0;
      1 1 0 0 0 1 0;
      0 0 0 0 0 0 1];
h2 = fspecial('disk', 3);
h3 = fspecial('gaussian', [5 5], 1);
h = conv2(conv2(h1,h2),h3);
h = h/sum(h(:));

% Noise;
sigma = 5/255;
q = 1/100; %1/50;



margin = ceil(max(size(h))*0.5)*2+2;


% Patch size
P = 12;
Px = P;
Py = 0.5*P;

X = zeros(Px^2, MAX_SAMPLES);
Y = zeros(Py^2, MAX_SAMPLES);

count = 1;

for ff = 1:2:1000,%length(dirlist),

    filename = dirlist(ff).name;

    I = imread(fullfile(IMG_DIR, filename));
    I = rgb2gray(I);
    I = im2double(I);   

    factor = 1024/max(size(I));
    I = imresize(I, factor, 'bicubic');
    I = min(1,max(0,I));
    I = I(1:floor(end/2)*2, 1:floor(end/2)*2);

    
    J = conv2(I, h, 'same');
    J = J + randn(size(J))*sigma;
    J = imresize(J, 0.5, 'bicubic');
    J = min(max(J, 0), 1);
    J = quant(J,q);

    I = I(1+margin:end-margin,1+margin:end-margin);
    J = J(1+margin/2:end-margin/2,1+margin/2:end-margin/2);
    
    
    imagesc(J); axis image; axis off; colormap gray; drawnow;

    N = MAX_SAMPLES_PER_IMAGE;
    i = floor(rand(N,1)*(size(I,1)-Px-4)/2+1)*2;
    j = floor(rand(N,1)*(size(I,2)-Px-4)/2+1)*2;

    ix = i; jx = j;
    iy = 0.5*i; jy = 0.5*j;


    for k=1:length(i),
        x = I([0:Px-1]+ix(k),[0:Px-1]+jx(k));
        y = J([0:Py-1]+iy(k),[0:Py-1]+jy(k));
        %imagesc([x kron(y,[1 1; 1 1])]);
        X(:,count) = x(:);
        Y(:,count) = y(:);
        count = count+1;
    end
    
    count


end

count = count-1;
idx = randperm(count);
X = X(:,idx(1:count));
Y = Y(:,idx(1:count));




idx1 = randperm(size(X,2));
Xp = X(:,idx1);
Yp = Y(:,idx1);


idx1 = round(rand(1.1e5,1)*(size(X,2)-1)+1);
idx2 = round(rand(1.1e5,1)*(size(X,2)-1)+1);
Xn = X(:,idx1);
Yn = Y(:,idx2);


%idx = reshape(1:Px^2,[Px Px]);
%idx = idx(1:2:end,1:2:end);
%idx = idx(:);

d = sqrt(mean((Y(:,idx1)-Y(:,idx2)).^2,1));
idx = find(d>0.05);
Xn = Xn(:,idx);
Yn = Yn(:,idx);




M = 64;
%K = [1000 15 300];
K = [1000 15 50];
Np_train = 1e4; %1e4;
Nn_train = .5e5; %2e5;
Np_test  = 1e4; %1e4;
Nn_test  = .5e5; %2e5;
alpha = 0.2;

% Similarity on X-Y
Xp_train = Xp(:,1:Np_train );
Yp_train = Yp(:,1:Np_train );
Xn_train = Xn(:,1:Nn_train );
Yn_train = Yn(:,1:Nn_train );
Xp_test  = Xp(:,[1:Np_test]+Np_train );
Yp_test  = Yp(:,[1:Np_test]+Np_train );
Xn_test  = Xn(:,[1:Nn_test]+Nn_train );
Yn_test  = Yn(:,[1:Nn_test]+Nn_train );
rand('seed',0);
randn('seed',0);
[PX,PY, v] = run_adaboost_cross(Xp_train,Yp_train,Xn_train,Yn_train, Xp_test,Yp_test,Xn_test,Yn_test, K(2:end),M,alpha);




ff = 50;
    filename = dirlist(ff).name;

    I = imread(fullfile(IMG_DIR, filename));
    I = rgb2gray(I);
    I = im2double(I);   

    factor = 1024/max(size(I));
    I = imresize(I, factor, 'bicubic');
    I = min(1,max(0,I));
    I = I(1:floor(end/2)*2, 1:floor(end/2)*2);

    
    J = conv2(I, h, 'same');
    J = J + randn(size(J))*sigma;
    J = imresize(J, 0.5, 'bicubic');
    J = min(max(J, 0), 1);
    J = quant(J,q);

    I = I(1+margin:end-margin,1+margin:end-margin);
    J = J(1+margin/2:end-margin/2,1+margin/2:end-margin/2);
    
    

    ix = 560; jx = 200;
    iy = 0.5*ix; jy = 0.5*jx;    
    n = 3;
    XX = [];
    for i=0:n-1,
        for j=0:n-1,
            x = I([0:Px-1]+ix+Px*i,[0:Px-1]+jx+Px*j);
            XX(:,end+1) = double(PX(:,1:end-1)*x(:) + PX(:,end) > 0);
            
        end
    end

    subplot(1,2,1);


    subplot(1,2,2);

    
    iy = 50;
    jy = 50;
    
    

    
%idx = reshape(1:Px^2,[Px Px]);
%idx = idx(1:2:end,1:2:end);
%idx = idx(:);

    n = 4;
    
        q = multiproj(I(ix+[0:Px*n-1],jx+[0:Px*n-1]),PX,Px,Px*n,2);
        %q = multiproj(J(iy+[0:Py*n-1],jy+[0:Py*n-1]),PY,Py,Py*n,1);
        %q = multiproj(J(iy+[0:Py*n-1],jy+[0:Py*n-1]),1:36,Py,Py*n,1);
%        q = multiproj(I,PX,Px,Px*n,2, ix,jx);
        D = nlfilter(J, [Py Py]*n, @(x)( sum(sum(multiproj(x,PY,Py,Py*n,1) ~= q)) / size(q,2) ) ); %@(x)(sum(q(:).*double(x(:)>0))));
%         D = nlfilter(J, [Py Py]*n, @(x)( sum(sum( (multiproj(x,1:36,Py,Py*n,1) - q).^2 )) ) ); %@(x)(sum(q(:).*double(x(:)>0))));
    

    figure(3);
    imagesc(I); 
    axis image; %axis off; 
    colormap gray; 
    h = patch([0 0 Px*n-1 Px*n-1]+jx, [0 Px*n-1 Px*n-1 0]+ix, [1 0 0]);
    set(h,'FaceColor','none','EdgeColor',[1 0 0]);
    drawnow;
    
    figure(2);
    imagesc(J); 
    axis image; %axis off; 
    colormap gray; 
    h = patch([0 0 Py*n-1 Py*n-1]+jy, [0 Py*n-1 Py*n-1 0]+iy, [1 0 0]);
    set(h,'FaceColor','none','EdgeColor',[1 0 0]);
    drawnow;
    figure(1);
    imagesc(D);
    axis image;
    h = patch([0 0 Py*n-1 Py*n-1]+jy, [0 Py*n-1 Py*n-1 0]+iy, [1 0 0]);
    set(h,'FaceColor','none','EdgeColor',[1 0 0]);
    drawnow;

         