function myplot_final2(clength)

% firstly, smh-mlbe
methodname = 'smh-mlbe';
nclass = 10;
lmperClass = 30;
load(sprintf('./results/res_flickr_%s_%d_%d_%d.mat',...
    methodname, nclass, lmperClass,clength));

premat1 = Probresult{1};
recmat1 = Probresult{2};
retmat1 = Probresult{3};
premat2 = Probresult{4};
recmat2 = Probresult{5};
retmat2 = Probresult{6};
% premat3 = Probresult{4};
% recmat3 = Probresult{5};
% retmat3 = Probresult{6};
methodname = 'bronstein';
load(sprintf('./results/res_flickr_%s_%d_%d_%d.mat',...
    methodname, nclass, lmperClass,clength));
premat3 = Probresult{1};
recmat3 = Probresult{2};
retmat3 = Probresult{3};

myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);
myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3);

