function myplot_prcurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3)

figure;
hold on;
mr = zeros(1,size(recmat1.XY,2));
mp = zeros(1,size(recmat1.XY,2));
for i = 1:size(recmat1.XY,2)
    idx = find(retmat1.XY(:,i)>0);
    mr(i) = mean(recmat1.XY(idx,i));
    mp(i) = mean(premat1.XY(idx,i));
end
plot(mr, mp, 'bo-', 'LineWidth',2,'MarkerSize',10);

mr = zeros(1,size(recmat2.XY,2));
mp = zeros(1,size(recmat2.XY,2));
for i = 1:size(recmat2.XY,2)
    idx = find(retmat2.XY(:,i)>0);
    mr(i) = mean(recmat2.XY(idx,i));
    mp(i) = mean(premat2.XY(idx,i));
end
plot(mr, mp, 'r*--', 'LineWidth',2,'MarkerSize',10);

mr = zeros(1,size(recmat3.XY,2));
mp = zeros(1,size(recmat3.XY,2));
for i = 1:size(recmat3.XY,2)
    idx = find(retmat3.XY(:,i)>0);
    mr(i) = mean(recmat3.XY(idx,i));
    mp(i) = mean(premat3.XY(idx,i));
end
plot(mr, mp, 'ks-.', 'LineWidth',2,'MarkerSize',10);

legend('MLBE','CVH','CMSSH');set(gca, 'FontSize',20);
%h = get(gca, 'legend');set(h, 'FontSize', 20);
title('Image Query vs. Text Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('Recall');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Precision');h = get(gca, 'ylabel');set(h, 'FontSize', 20);

figure;
hold on;
mr = zeros(1,size(recmat1.YX,2));
mp = zeros(1,size(recmat1.YX,2));
for i = 1:size(recmat1.YX,2)
    idx = find(retmat1.YX(:,i)>0);
    mr(i) = mean(recmat1.YX(idx,i));
    mp(i) = mean(premat1.YX(idx,i));
end
plot(mr, mp, 'bo-', 'LineWidth',2,'MarkerSize',10);

mr = zeros(1,size(recmat2.YX,2));
mp = zeros(1,size(recmat2.YX,2));
for i = 1:size(recmat2.YX,2)
    idx = find(retmat2.YX(:,i)>0);
    mr(i) = mean(recmat2.YX(idx,i));
    mp(i) = mean(premat2.YX(idx,i));
end
plot(mr, mp, 'r*--', 'LineWidth',2,'MarkerSize',10);

mr = zeros(1,size(recmat3.YX,2));
mp = zeros(1,size(recmat3.YX,2));
for i = 1:size(recmat3.YX,2)
    idx = find(retmat3.YX(:,i)>0);
    mr(i) = mean(recmat3.YX(idx,i));
    mp(i) = mean(premat3.YX(idx,i));
end
plot(mr, mp, 'ks-.', 'LineWidth',2,'MarkerSize',10);

legend('MLBE','CVH','CMSSH');set(gca, 'FontSize',20);
%h = get(gca, 'legend');set(h, 'FontSize', 20);
title('Text Query vs. Image Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('Recall');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Precision');h = get(gca, 'ylabel');set(h, 'FontSize', 20);