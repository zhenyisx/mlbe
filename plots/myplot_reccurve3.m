function myplot_reccurve3(premat1, recmat1, retmat1,premat2, recmat2, retmat2,premat3, recmat3, retmat3)

figure;
hold on;
plot(mean(retmat1.XY,1), mean(recmat1.XY,1), 'bo-', 'LineWidth',2,'MarkerSize',10);
plot(mean(retmat2.XY,1), mean(recmat2.XY,1), 'r*--', 'LineWidth',2,'MarkerSize',10);
plot(mean(retmat3.XY,1), mean(recmat3.XY,1), 'ks-.', 'LineWidth',2,'MarkerSize',10);

legend('MLBE','CVH','CMSSH');set(gca, 'FontSize',20);
%h = get(gca, 'legend');set(h, 'FontSize', 20);
title('Image Query vs. Text Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('No. of Retrieved Points');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Recall');h = get(gca, 'ylabel');set(h, 'FontSize', 20);

figure;
hold on;
plot(mean(retmat1.YX,1), mean(recmat1.YX,1), 'bo-', 'LineWidth',2,'MarkerSize',10);
plot(mean(retmat2.YX,1), mean(recmat2.YX,1), 'r*--', 'LineWidth',2,'MarkerSize',10);
plot(mean(retmat3.YX,1), mean(recmat3.YX,1), 'ks-.', 'LineWidth',2,'MarkerSize',10);

legend('MLBE','CVH','CMSSH');set(gca, 'FontSize',20);
%h = get(gca, 'legend');set(h, 'FontSize', 20);
title('Text Query vs. Image Database');h = get(gca, 'title');set(h, 'FontSize', 20);
xlabel('No. of Retrieved Points');h = get(gca, 'xlabel');set(h, 'FontSize', 20);
ylabel('Recall');h = get(gca, 'ylabel');set(h, 'FontSize', 20);