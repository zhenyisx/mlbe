function myresult_comp

% script of drawing figures
addpath('./results');
addpath('./utils');

% %%%%%%%%%%%%%  MAP on UCSD, 500 Landmarks %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ms = []; ss = [];
% load('res_ucsd_cmssh-toy_normal_linear_euc_2-20both_zero.mat'); 
% load('res_ucsd_cmssh-toy_normal_linear_cos_2-20both_zero.mat'); 
% load('res_ucsd_bro-toy_normal_linear_euc_2-20both_zero.mat'); 
load('res_ucsd_bro-toy_normal_linear_cos_2-20both_zero.mat'); 
% load('res_ucsd_mlbe-toy_normal_linear_cos_2-20both_zero.mat'); 
% load('res_ucsd_mlbe-toy_normal_linear_cos_2-20both_zero.mat'); 
xx = Probparam.codelength
idx = 1:length(xx);
% MAP
fprintf('MAP@%d',Probparam.hammingrank);
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_mapmat.XY,idx); %rank based map for XX
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_mapmat.YX,idx); %rank based map for YY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_mapmat.XX,idx); %rank based map for XY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_mapmat.YY,idx); %rank based map for YX
ms
ss

ms = []; ss = [];
fprintf('Precision@%d',Probparam.hammingrank);
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_premat.XY,idx); %rank based map for XX
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_premat.YX,idx); %rank based map for YY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_premat.XX,idx); %rank based map for XY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_premat.YY,idx); %rank based map for YX
ms
ss

% Recall@100
ms = []; ss = [];
fprintf('Recall@%d',Probparam.hammingrank);
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_recmat.XY,idx); %rank based map for XX
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_recmat.YX,idx); %rank based map for YY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_recmat.XX,idx); %rank based map for XY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.rank_recmat.YY,idx); %rank based map for YX
ms
ss

% Precision@2
ms = []; ss = [];
fprintf('Precision@%d',Probparam.hammingradius);
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_premat.XY,idx); %rank based map for XX
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_premat.YX,idx); %rank based map for YY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_premat.XX,idx); %rank based map for XY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_premat.YY,idx); %rank based map for YX
ms
ss

% Recall@2
ms = []; ss = [];
fprintf('Recall@%d',Probparam.hammingradius);
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_recmat.XY,idx); %rank based map for XX
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_recmat.YX,idx); %rank based map for YY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_recmat.XX,idx); %rank based map for XY
[ms(end+1,:), ss(end+1,:)] = getresult(Probresult.radius_recmat.YY,idx); %rank based map for YX
ms
ss




function [m s] = getresult(X,idx)
% get mean and std of each column
m = mean(X,1);
s = std(X, [], 1);
if nargin >1    
    m = m(idx);
    s = s(idx);
end

