function myplot_final(methodname, nclass, lmperClass, clength)


load(sprintf('./results/res_flickr_%s_%d_%d_%d.mat',...
    methodname, nclass, lmperClass,clength));

% hyper

% plot the results
if size(Probresult,1)>=9
    premat = Probresult{1};
    recmat = Probresult{2};
    retmat = Probresult{3};
    premat1 = Probresult{4};
    recmat1 = Probresult{5};
    retmat1 = Probresult{6};
    rank_premat = Probresult{7};
    rank_recmat = Probresult{8};
    rank_mapmat = Probresult{9};
%     myplot_prcurve(premat, recmat, retmat);
%     myplot_prcurve(premat1, recmat1, retmat1);
%     myplot_reccurve(premat, recmat, retmat);
%     myplot_reccurve(premat1, recmat1, retmat1);
%     [h,p] = ttest(rank_mapmat.XY(:,1),rank_mapmat.XY(:,2))
%     [h,p] = ttest(rank_mapmat.YX(:,1),rank_mapmat.YX(:,2)) 
    mean(rank_mapmat.XY,1)
    std(rank_mapmat.XY,[],1)
    mean(rank_mapmat.YX,1)
    std(rank_mapmat.YX,[],1)
%     myplot_prcurve2(premat, recmat, retmat, premat1, recmat1, retmat1);
else
    premat = Probresult{1};
    recmat = Probresult{2};
    retmat = Probresult{3};
    rank_premat = Probresult{4};
    rank_recmat = Probresult{5};
    rank_mapmat = Probresult{6};
%     mean(premat.XY(:,3),1)
%     mean(premat.YX(:,3),1)
%     myplot_prcurve(premat, recmat, retmat);
%     myplot_reccurve(premat, recmat, retmat);
    mean(rank_mapmat.XY,1)
    std(rank_mapmat.XY,[],1)
    mean(rank_mapmat.YX,1)
    std(rank_mapmat.YX,[],1)
%     myplot_prcurve(premat, recmat, retmat);
end