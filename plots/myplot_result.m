function myplot_result()

mxy10 = [0.1763 0.1829 0.1716 0.1762];
sxy10 = [0.0179 0.0198 0.0159 0.0142];
myx10 = [0.1594 0.1831 0.1880 0.1869];
syx10 = [0.0323 0.0195 0.0245 0.0281];

multierrorbar([mxy10;myx10], zeros(size([sxy10;syx10])));

end