
mlbe_entry: the entry file of MLBE project.
mlbe_main: the main computation file of MLBE project.

/plot: contains files for ploting the results.
/utils: contains files for small functions.
/brostein_mmssh: contains files for the CMSSH method.