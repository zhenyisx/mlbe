%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Distributed under GNU General Public License (see license.txt for details).
%
% Copyright (c) 2012 Linus ZHEN Yi
% All Rights Reserved.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The main function for MLBE
% Input:
%       ProbParam.datafile          : .mat data file
%       ProbParam.neighborfile      : .mat neighborhood file
%       ProbParam.indexfile         : .mat random index file
%       ProbParam.evaltype          : 'rank' or 'radius' or 'both'
%       ProbParam.hammingrank       : rank positions for evaluation
%       ProbParam.hammingradius     : radii for evaluation
%       ProbParam.codelength        : # of bits of binary codes
%       ProbParam.repeats           : # of experiments, each experiments corresponds to a random landmark set
%       ProbParam.methodname        : 'smhbase', 'ksmh' or 'rksmh'
%       ProbParam.threshname        : 'zero','mean','median','quater-mean','quater-median','learn'
%       ProbParam.landmarksize      : size of landmark set
%       Probparam.subtrain          : use a subset for training, -1 indicate use all training set
% output:
%   ProbResults: results for evaluation by ranking and by radius
%   hyper: hyperparameters of model
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ProbResults, hyper] = mlbe_main(ProbParam)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Preprocess data
	load(ProbParam.datafile);
    % mean-center the features
    x_mean = mean(feax_tr,2);
    y_mean = mean(feay_tr,2);
    feax_tr = double(bsxfun(@minus, feax_tr, x_mean)); %feax_tr = feax_tr-repmat(x_mean,1,Ntrain);
    feax_te = double(bsxfun(@minus, feax_te, x_mean)); %feax_te = feax_te-repmat(x_mean,1,Ntest);
    feay_tr = double(bsxfun(@minus, feay_tr, y_mean)); %feay_tr = feay_tr-repmat(y_mean,1,Ntrain);
    feay_te = double(bsxfun(@minus, feay_te, y_mean)); %feay_te = feay_te-repmat(y_mean,1,Ntest);
    if length(ProbParam.codelength)>1
        error('wrong code length. Should be a scaler!');
    else
        curr_length = ProbParam.codelength;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Initialize temporary results
    if strcmp(ProbParam.methodname,'cvh') || strcmp(ProbParam.methodname,'bronstein')
        premat.XY = zeros(1, curr_length+1);
        premat.YX = zeros(1, curr_length+1);
        recmat.XY = zeros(1, curr_length+1);
        recmat.YX = zeros(1, curr_length+1);
        retmat.XY = zeros(1, curr_length+1);
        retmat.YX = zeros(1, curr_length+1);
        rank_mapmat.XY = zeros(1, 1);
        rank_mapmat.YX = zeros(1, 1);
        rank_premat.XY = zeros(1, 1);
        rank_premat.YX = zeros(1, 1);
        rank_recmat.XY = zeros(1, 1);
        rank_recmat.YX = zeros(1, 1);
    else
        premat.XY = zeros(ProbParam.repeats, curr_length+1);
        premat.YX = zeros(ProbParam.repeats, curr_length+1);
        recmat.XY = zeros(ProbParam.repeats, curr_length+1);
        recmat.YX = zeros(ProbParam.repeats, curr_length+1);
        retmat.XY = zeros(ProbParam.repeats, curr_length+1);
        retmat.YX = zeros(ProbParam.repeats, curr_length+1);
        rank_mapmat.XY = zeros(ProbParam.repeats, 2);
        rank_mapmat.YX = zeros(ProbParam.repeats, 2);
        rank_premat.XY = zeros(ProbParam.repeats, 2);
        rank_premat.YX = zeros(ProbParam.repeats, 2);
        rank_recmat.XY = zeros(ProbParam.repeats, 2);
        rank_recmat.YX = zeros(ProbParam.repeats, 2);
        rank_accmat.XY = zeros(ProbParam.repeats, 2);
        rank_accmat.YX = zeros(ProbParam.repeats, 2);
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Train projection matrix
    
        
    if strcmp(ProbParam.methodname,'cvh')% CVH
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % covariance matrices (note that this should be on train set)
        lambda = 1e-6;
        Cxx = feax_tr*feax_tr'+lambda*eye(size(feax_tr,1));
        Cyy = feay_tr*feay_tr'+lambda*eye(size(feay_tr,1));
        Cxy = feax_tr*feay_tr';

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % train CCA
        [Wx, Wy] = trainCCA(Cxx, Cyy, Cxy, curr_length);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Compress dataset: B = [nWords(ceil(Nbits/8)), Nsamples]
        [Bx_tr] = compress2code(feax_tr, Wx, 'zero', 0);% Xtr
        [Bx_te] = compress2code(feax_te, Wx, 'zero', 0);% Xte
        [By_tr] = compress2code(feay_tr, Wy, 'zero', 0);% Ytr
        [By_te] = compress2code(feay_te, Wy, 'zero', 0);% Yte

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Evaluate codes
        [Dhamm] = computeHdist(Bx_tr, By_tr, Bx_te, By_te);
        clear Bx_tr By_tr Bx_te By_te% DN.XY = Deig; DN.YX = Deig;
        DN = (gnd_te'* gnd_tr)>0;
        size(DN')
        size(Dhamm.XY')
        [premat.XY(1,:), recmat.XY(1,:), retmat.XY(1,:), rank_premat.XY(1, 1),rank_recmat.XY(1, 1),rank_mapmat.XY(1, 1)]=...
            totalevaluationc(DN', Dhamm.XY', curr_length, ProbParam.hammingrank);
        [premat.YX(1,:), recmat.YX(1,:), retmat.YX(1,:), rank_premat.YX(1, 1),rank_recmat.YX(1, 1),rank_mapmat.YX(1, 1)]=...
            totalevaluationc(DN', Dhamm.YX', curr_length, ProbParam.hammingrank);
    elseif strcmp(ProbParam.methodname, 'mlbe') % MLBE with input (CVH)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % prepare training data
        % index of landmark points
        load(ProbParam.rdidxfile);            
        % fix the landmarks (kmedoids)
        iXlmidx = Xlmidx(:,1);
        if ~isempty(Xtridx)
            iXtridx = Xtridx(:,1);
        else
            iXtridx = [];
        end
        iYlmidx = Ylmidx(:,1);
        if ~isempty(Ytridx)
            iYtridx = Ytridx(:,1);
        else
            iYtridx = [];
        end
        load(ProbParam.simfile);
        clear SXY_tr SXY_te
        SXY_tr = (gnd_tr'*gnd_tr)>0;
        SXY_te = (gnd_te'*gnd_tr)>0;
        
        % use flitered sim files
        SX_tr = SX_tr.*SXY_tr;% non-neighbor points have similarity 0
        SY_tr = SY_tr.*SXY_tr;
        SX_te = SX_te.*SXY_te;
        SY_te = SY_te.*SXY_te;
        
        % get random cross-modal observations
        load(ProbParam.obfile);
        OXY_tr = OXYs{1};clear OXYs;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % use CVH as initialization
        % covariance matrices
        lambda = 1e-6;
        Cxx = feax_tr*feax_tr'+lambda*eye(size(feax_tr,1));
        Cyy = feay_tr*feay_tr'+lambda*eye(size(feay_tr,1));
        Cxy = feax_tr*feay_tr';
        % train CCA on the whole data
        [WMx, WMy] = trainCCA(Cxx, Cyy, Cxy, max(ProbParam.codelength));
%         size(Wx)
%         size(Wy)
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % train and test MLBE
        burninrepeats = ProbParam.repeats; % MLBE run multiple repeats
        hyper.used = 0;
        for r = 1:burninrepeats
            % use CVH initialization
            U_tr = 2*double(bsxfun(@gt,WMx'*feax_tr,0))-1; 
            U_te = 2*double(bsxfun(@gt,WMx'*feax_te,0))-1;
            V_tr = 2*double(bsxfun(@gt,WMy'*feay_tr,0))-1;
            V_te = 2*double(bsxfun(@gt,WMy'*feay_te,0))-1;

            % train MBLE on landmark set
            hyper.refsize = curr_length;% subsampling features to update

            % initialize parameters
            hyper.thetax = 1e-2; hyper.thetay = 1e-2;
            hyper.phix = 1e-2; hyper.phiy = 1e-2;hyper.phixy = 1e-2;
            hyper.alphax = 2; hyper.betax = 1;
            hyper.alphay = 2; hyper.betay = 1;
            hyper.aphi = 2; hyper.bphi = 1;
            hyper.cphi = 2; hyper.dphi = 1;
            hyper.ephi = 2; hyper.fphi = 1;
            hyper.atheta = 1; hyper.btheta = 1;
            hyper.ctheta = 1; hyper.dtheta = 1;
                
            % prepare landmarks
            SXR = SX_tr(iXlmidx,iXlmidx);%SXY_tr(iXlmidx,iXlmidx);%.*
            SYR = SY_tr(iYlmidx,iYlmidx);%.*SXY_tr(iYlmidx,iYlmidx);
            SXYR = SXY_tr(iXlmidx,iYlmidx);
            OXYR = OXY_tr(iXlmidx,iYlmidx);
            UR1 = U_tr(:,iXlmidx);
            VR1 = V_tr(:,iYlmidx);
            
            % train MLBE on landmarks
            [UR, VR, Wx, Wy, wxy, hyper, objvalue] = trainMLBE(SXR, SYR, SXYR, OXYR, hyper, curr_length, UR1, VR1);
            Bx_lm = compactbit(UR>0);
            By_lm = compactbit(VR>0);

            % apply on the whole (training and testing) data for X
            params.UR = UR; params.VR = VR; 
            params.Wx = Wx; params.wxy = wxy;
            SXtr = SX_tr(iXtridx,iXlmidx);%SXY_tr(iXtridx,iXlmidx); %.*
            SXYtr = SXY_tr(iXtridx,iYlmidx); 
            OXYtr = OXY_tr(iXtridx,iYlmidx);
            Utr1 = U_tr(:, iXtridx);
            [Bx_tr] = applyMLBE('x', SXtr, SXYtr, OXYtr, params, hyper, curr_length, Utr1);
            Bx_tr = [Bx_tr Bx_lm];
            SXte = SX_te(:,iXlmidx);%SXY_te(:,iXlmidx); % .* here, for test points, we should not consider XY
            SXYte = SXY_te(:,iYlmidx); 
            OXYte = sparse(zeros(size(SXYte))); %OXYte = OXY_te(:,iYlmidx); 
            [Bx_te] = applyMLBE('x', SXte, SXYte, OXYte, params, hyper, curr_length, U_te);

            % apply on the whole (training and testing) data for Y
            params.UR = VR; params.VR = UR; 
            params.Wx = Wy; params.wxy = wxy;
            SXtr = SY_tr(iYtridx,iYlmidx);%.*SXY_tr(iYtridx,iYlmidx)
            SXYtr = SXY_tr(iXlmidx,iYtridx)'; 
            OXYtr = OXY_tr(iXlmidx,iYtridx)';
            Vtr1 = V_tr(:, iYtridx);
            [By_tr] = applyMLBE('y', SXtr, SXYtr, OXYtr, params, hyper, curr_length, Vtr1);
            By_tr = [By_tr By_lm];
            SXte = SY_te(:,iYlmidx); %.*SXY_te(:,iYlmidx)
            SXYte = SXY_te(:,iXlmidx); %OXYte = zeros(size(SXYte));
            OXYte = sparse(zeros(size(SXYte))); %OXYte = OXY_te(:,iXlmidx);
            [By_te] = applyMLBE('y', SXte, SXYte, OXYte, params, hyper, curr_length,V_te);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Evaluate codes
            DN.XY = SXY_te(:, [iYtridx;iYlmidx]);
            DN.YX = SXY_te(:, [iXtridx;iXlmidx]);
            [Dhamm] = computeHdist(Bx_tr, By_tr, Bx_te, By_te); clear Bx_tr By_tr Bx_te By_te
            [premat.XY(r,:), recmat.XY(r,:), retmat.XY(r,:), rank_premat.XY(r, 1),rank_recmat.XY(r, 1),rank_mapmat.XY(r, 1)]=...
                totalevaluationc(DN.XY', Dhamm.XY', curr_length, ProbParam.hammingrank);
            [premat.YX(r,:), recmat.YX(r,:), retmat.YX(r,:), rank_premat.YX(r, 1),rank_recmat.YX(r, 1),rank_mapmat.YX(r, 1)]=...
                totalevaluationc(DN.YX', Dhamm.YX', curr_length, ProbParam.hammingrank);
        end

    else
        disp('Your method is not supported.');
    end
  
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Save results
    ProbResults = cell(6,1);
    ProbResults{1} = premat;
    ProbResults{2} = recmat;
    ProbResults{3} = retmat;
    ProbResults{4} = rank_premat;
    ProbResults{5} = rank_recmat;
    ProbResults{6} = rank_mapmat;
    hyper = [];


end

function [Dhamm] = computeHdist(Bx_tr, By_tr, Bx_te, By_te)
%     Dhamm.XX = hammingDist2(Bx_te, Bx_tr);% image query - image database
%     Dhamm.YY = hammingDist2(By_te, By_tr);% text query  - text database
    Dhamm.XY = hammingDist2(Bx_te, By_tr);% image query  - text database
    Dhamm.YX = hammingDist2(By_te, Bx_tr);% text query - image database
end